FROM ubuntu:18.04

RUN \
# Install required Ubuntu tools \
apt-get update && \
apt-get install -y curl git locales xz-utils libgmp-dev zlib1g-dev libtinfo-dev build-essential && \
# Set up locale (required later for successful Haddoc generation) \
locale-gen 'en_AU.UTF-8' && \
export LC_ALL='en_AU.UTF-8' && \
# Install Haskell stack \
curl -L https://get.haskellstack.org/stable/linux-x86_64.tar.gz | tar xz --wildcards --strip-components=1 -C /usr/local/bin '*/stack' && \
# Clone the workshop and fetch dependencies \
git clone https://github.com/lancelet/space-workshop.git /opt/space-workshop && \
cp /opt/space-workshop/stack.yaml /root/.stack.yaml && \
cd /opt/space-workshop && \
stack --no-terminal --install-ghc test --only-dependencies && \
stack --no-terminal hoogle --setup && \
# Clean up caches \
rm -rf /var/lib/apt/lists
