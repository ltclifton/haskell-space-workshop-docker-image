#!/bin/sh
dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)
version=$(sed -e 's/^[ \t]*//' < "${dir}"/VERSION)
docker login -u lancelet -p ${DOCKER_HUB_PASSWORD}
docker load -i img.tar
docker push lancelet/hs-spaceflight-2019:"${version}"
